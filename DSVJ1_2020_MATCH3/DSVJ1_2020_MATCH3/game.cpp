#include "game.h"

using namespace match3;
using namespace gameScenes;

namespace match3 {

	namespace game {
		bool exitGame = false;

		void run() {
			init();
			SetWindowMonitor(0);
			while (!WindowShouldClose() && exitGame != true) {
				update();
				draw();
			}
			deinit();
		}

		static void init() {
			gameResolution::init();
			InitWindow(gameResolution::myRes.screenWitdh, gameResolution::myRes.screenHeight, "ENDLESS");

			SetTargetFPS(60);
			SetExitKey(KEY_DELETE);

			gameScenes::currentScene = Scene::play;
			exitGame = false;

			gamePlay::init();
			//menu::init();
			//sounds::init();
			//music::init();
			//button::init();
			//quit::init();
			//player::init();
		}

		static void update() {
			switch (currentScene)
			{
			case Scene::menu:
				gameMenu::update();
				break;
			
			case Scene::play:
				gamePlay::update();
				break;
			
			case Scene::tutorial:
				gameTutorial::update();
				break;
			
			case Scene::credits:
				gameCredits::update();
				break;
			
			case Scene::quit:
				gameQuit::update();
				break;		
			}
		}

		static void draw() {
			BeginDrawing();
			ClearBackground(BLACK);

			switch (currentScene)
			{
			case Scene::menu:
				gameMenu::draw();
				break;
			
			case Scene::play:
				gamePlay::draw();
				break;
			
			case Scene::tutorial:
				gameTutorial::draw();
				break;
			
			case Scene::credits:
				gameCredits::draw();
				break;
			
			case Scene::quit:
				gameQuit::draw();
				break;	
			}
			EndDrawing();
		}

		static void deinit() {

			CloseWindow();
		}
	}
}