#ifndef CREDITS_H
#define CREDITS_H

namespace match3 {

	namespace gameCredits {
				
		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif
