#include "scenes.h"

namespace match3 {

	namespace gameScenes {

		Scene currentScene;
		Scene menuScreen;
		Scene playScreen;
		Scene tutorialScreen;
		Scene creditsScreen;
		Scene pauseScreen;
		Scene quitScreen;		

		void init() {
			currentScene = Scene::menu;
			menuScreen = Scene::menu;
			playScreen = Scene::play;
			tutorialScreen = Scene::tutorial;
			creditsScreen = Scene::credits;
			pauseScreen = Scene::pauseScreen;
			quitScreen = Scene::quit;			
		}

		void update() {

		}

		void draw() {

		}

		void deinit() {

		}
	}
}