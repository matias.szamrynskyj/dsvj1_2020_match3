#include "gameplay.h"
#include "cubes.h"
#include "player.h"

using namespace match3;
using namespace cubes;
using namespace player;

namespace match3 {

	namespace gamePlay {

		const int blocksIL1 = 8;
		const int blocksJL1 = 8;

		int contYL1;
		int contXL1;

		int idI;
		int idJ;

		float cubeWidth;
		float cubeHeight;

		bool onPoint;
		bool quitFor;

		gameCubes* levelOneCubes[blocksIL1][blocksJL1];
		myPlayer* myMouse;

		void init() {
			contYL1 = 0;
			contXL1 = 0;

			idI = 0;
			idJ = 0;

			cubeWidth = 100.0f;
			cubeHeight = 100.0f;

			onPoint = false;
			quitFor = true;

			myMouse = new myPlayer();

			for (int i = 0; i < blocksIL1; i++) {

				//setea el espacio entre cuadrados en y
				if (i != 0)
					contYL1 += cubeHeight + 20.0f; //espacio entre bloques				
				else
					contYL1 = 50.0f; //posicion inicial de y				

				for (int j = 0; j < blocksJL1; j++) {

					//setea el espacio entre cuadrados en x
					if (j != 0)
						contXL1 += cubeWidth + 20.0f; //espacio entre bloques
					else
						contXL1 = GetScreenWidth() / 2 - 450.0f; //posicion inicial de x

					levelOneCubes[i][j] = new gameCubes();
					levelOneCubes[i][j]->setDimensions(cubeWidth, cubeHeight);
					levelOneCubes[i][j]->setColor(BLUE);
					levelOneCubes[i][j]->setID(i, j);
					levelOneCubes[i][j]->setXY(contXL1, contYL1);
				}
			}
		}

		void update() {

			if (quitFor) {

				for (int i = 0; i < blocksIL1; i++) {
					for (int j = 0; j < blocksJL1; j++) {

						if (myMouse->checkCol(levelOneCubes[i][j])) {
							onPoint = true;
							idI = levelOneCubes[i][j]->getIDI();
							idJ = levelOneCubes[i][j]->getIDJ();
							quitFor = false;
							break;
						}
						else
							onPoint = false;

						if (onPoint)
							levelOneCubes[i][j]->showID();
					}

				}
			}		

			myMouse->moveX(levelOneCubes[idI][idJ]);
			myMouse->moveY(levelOneCubes[idI][idJ]);

			if (IsMouseButtonReleased)
				quitFor = true;
		}

		void draw() {

			for (int i = 0; i < blocksIL1; i++) {
				for (int j = 0; j < blocksJL1; j++) {

					if (levelOneCubes[i][j] != NULL) {
						levelOneCubes[i][j]->draw();
					}
				}
			}

		}

		void deinit() {

		}

	}
}