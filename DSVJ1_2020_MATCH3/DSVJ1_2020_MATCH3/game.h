#ifndef GAME_H
#define GAME_H

#include "raylib.h"
#include "gameResolution.h"
#include "scenes.h"
#include "menu.h"
#include "gameplay.h"
#include "tutorial.h"
#include "credits.h"
#include "quit.h"

namespace match3 {

	namespace game {

		extern bool exitGame;

		void run();

		static void init();
		static void update();
		static void draw();
		static void deinit();
	}
}
#endif



