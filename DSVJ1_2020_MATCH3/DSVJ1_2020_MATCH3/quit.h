#ifndef QUIT_H
#define QUIT_H

namespace match3 {

	namespace gameQuit {	

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif
