#ifndef PLAY_H
#define PLAY_H

namespace match3 {

	namespace gamePlay {
				

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif