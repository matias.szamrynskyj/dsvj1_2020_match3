#include "player.h"

namespace match3 {

	namespace player {

		bool myPlayer::checkCol(gameCubes* rec){			
			return CheckCollisionPointRec(GetMousePosition(), rec->getCube());			
		}

		void myPlayer::moveX(gameCubes* rec){
			if (checkCol(rec))
				if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))			
					rec->setX(GetMouseX() - rec->getWidth()/2);
		}

		void myPlayer::moveY(gameCubes* rec){
			if (checkCol(rec))
				if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
					rec->setY(GetMouseY() - rec->getHeight()/2);
		}
	}
}