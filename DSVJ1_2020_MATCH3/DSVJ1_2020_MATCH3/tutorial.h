#ifndef	TUTORIAL_H
#define TUTORIAL_H

namespace match3 {

	namespace gameTutorial {		

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif
