#ifndef CUBES_H
#define CUBES_H

#include <iostream>
#include "raylib.h"

using namespace std;

namespace match3 {

	namespace cubes {
		class gameCubes {
		private:
			Rectangle myRec;
			Color cubeColor;
			int idI;
			int idJ;
		public:
			gameCubes();
			~gameCubes();
			void setWidth(float witdh);
			void setHeight(float height);
			void setDimensions(float witdh, float height);
			void setColor(Color color);
			void setX(int x);
			void setY(int y);
			void setXY(int x, int y);
			void setID(int i, int j);

			float getWidth();
			float getHeight();
			float getX();
			float getY();
			int getIDI();
			int getIDJ();
			Rectangle getCube();

			void showID();
			void draw();
		};				
		
		//void init();
		//void update();
		//void draw();
		//void deinit();
	}
}
#endif
