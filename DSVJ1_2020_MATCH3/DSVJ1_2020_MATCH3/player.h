#ifndef PLAYER_H
#define PLAYER_H

#include "raylib.h"
#include "cubes.h"

using namespace match3;
using namespace cubes;

namespace match3 {

	namespace player {

		class myPlayer {
		private:

		public:
			bool checkCol(gameCubes* rec);
			void moveX(gameCubes* rec);
			void moveY(gameCubes* rec);
		};
	}
}
#endif



