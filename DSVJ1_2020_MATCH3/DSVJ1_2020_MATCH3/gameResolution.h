#ifndef GAMERESOLUTION_H
#define GAMERESOLUTION_H

#include "raylib.h"

namespace match3 {

	namespace gameResolution {

		struct gameRes {
			int screenWitdh;
			int screenHeight;
		};

		extern gameRes myRes;

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif

