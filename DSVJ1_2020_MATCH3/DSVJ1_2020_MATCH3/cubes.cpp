#include "cubes.h"

namespace match3 {
	namespace cubes {
		gameCubes::gameCubes() {
			myRec.width = 100.0f;
			myRec.height = 100.0f;
			myRec.x = 1.0f;
			myRec.y= 1.0f;			
		}
		gameCubes::~gameCubes() {

		}
		void gameCubes::setWidth(float witdh) {
			myRec.width = witdh;
		}
		void gameCubes::setHeight(float height) {
			myRec.height = height;
		}
		void gameCubes::setDimensions(float witdh, float height){
			setWidth(witdh);
			setHeight(height);
		}
		void gameCubes::setColor(Color color) {
			cubeColor = color;
		}
		void gameCubes::setX(int x) {
			myRec.x = x;
		}
		void gameCubes::setY(int y) {
			myRec.y = y;
		}
		void gameCubes::setXY(int x, int y) {
			setX(x);
			setY(y);
		}

		void gameCubes::setID(int i, int j)	{
			idI = i;
			idJ = j;
		}

		float gameCubes::getWidth()	{
			return myRec.width;
		}

		float gameCubes::getHeight(){
			return myRec.height;
		}

		float gameCubes::getX()	{
			return myRec.x;
		}

		float gameCubes::getY()	{
			return myRec.y;
		}

		int gameCubes::getIDI()
		{
			return idI;
		}

		int gameCubes::getIDJ()
		{
			return idJ;
		}

		Rectangle gameCubes::getCube(){
			return myRec;
		}

		void gameCubes::showID(){
			DrawText(FormatText("i: %i", idI), 20, 10, 50, GREEN);
			DrawText(FormatText("j: %i", idJ), 20, 60, 50, GREEN);
			cout << "i: " << idI << "j: " << idJ << endl;
		}

		void gameCubes::draw()	{			
			DrawRectangle(myRec.x, myRec.y, myRec.width, myRec.height, cubeColor);
		}
	}
}